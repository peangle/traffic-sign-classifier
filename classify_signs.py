#Imports
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import gzip
import os
import sys
import time
import pickle
import numpy
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
from sklearn.utils import shuffle
from PIL import Image
import cv2
from os import listdir
from os.path import isfile, join
from tensorflow.contrib.layers import flatten

training_file = "traffic-signs-data/train.p"
testing_file = "traffic-signs-data/test.p"

with open(training_file, mode='rb') as f:
    train = pickle.load(f)
with open(testing_file, mode='rb') as f:
    test = pickle.load(f)

train_data, train_labels = shuffle(train['features'], train['labels'])
test_data, test_labels = shuffle(test['features'], test['labels'])

print(train_data.shape)
print(test_data.shape)


### Replace each question mark with the appropriate value.

n_train = train_data.shape[0]
n_test = test_data.shape[0]
image_shape = train_data[0].shape
n_classes = numpy.unique(train_labels).shape[0]

print("Number of training examples =", n_train)
print("Number of testing examples =", n_test)
print("Image data shape =", image_shape)
print("Number of classes =", n_classes)


### Data exploration visualization goes here.
### Feel free to use as many code cells as needed.
import random
import matplotlib.pyplot as plt

index = random.randint(0, len(train_data))
image = train_data[index]
print(train_labels[index])
plt.imshow(image)
plt.show()

### Preprocess the data here.
### Feel free to use as many code cells as needed.
def normalize_data(data):
    return numpy.array([numpy.float32((datum[:]-numpy.mean(datum))/numpy.std(datum)) for datum in data])

### Generate data additional data (OPTIONAL!)
### and split the data into training/validation/testing sets here.
### Feel free to use as many code cells as needed.
validation_size = int(train_data.shape[0] * 0.2)
validation_data = train_data[:validation_size, ...]
validation_labels = train_labels[:validation_size]
train_data = train_data[validation_size:, ...]
train_labels = train_labels[validation_size:]
train_size = train_labels.shape[0]

IMAGE_SIZE = 32
NUM_CHANNELS = 3
NUM_LABELS = 43
SEED = 66478
BATCH_SIZE = 512
NUM_EPOCHS = 5

### Define your architecture here.
### Feel free to use as many code cells as needed.
def data_type():
    return tf.float32

train_data_node = tf.placeholder(
      data_type(),
      shape=(None, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))
train_labels_node = tf.placeholder(tf.int64, shape=(None,))
eval_data = tf.placeholder(
      data_type(),
      shape=(None, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))

conv1_weights = tf.Variable(
      tf.truncated_normal([5, 5, NUM_CHANNELS, 32],
                          stddev=0.1,
                          seed=SEED, dtype=data_type()))
conv1_biases = tf.Variable(tf.zeros([32], dtype=data_type()))
conv2_weights = tf.Variable(tf.truncated_normal(
      [5, 5, 32, 64], stddev=0.1,
      seed=SEED, dtype=data_type()))
conv2_biases = tf.Variable(tf.constant(0.1, shape=[64], dtype=data_type()))

conv3_weights = tf.Variable(tf.truncated_normal(
      [5, 5, 64, 128], stddev=0.1,
      seed=SEED, dtype=data_type()))
conv3_biases = tf.Variable(tf.constant(0.1, shape=[128], dtype=data_type()))

fc1_weights = tf.Variable(
      tf.truncated_normal([IMAGE_SIZE // 8 * IMAGE_SIZE // 8 * 128, 512],
                          stddev=0.1,
                          seed=SEED,
                          dtype=data_type()))
fc1_biases = tf.Variable(tf.constant(0.1, shape=[512], dtype=data_type()))
fc2_weights = tf.Variable(tf.truncated_normal([512, 256],
                                                stddev=0.1,
                                                seed=SEED,
                                                dtype=data_type()))
fc2_biases = tf.Variable(tf.constant(
      0.1, shape=[256], dtype=data_type()))

fc3_weights = tf.Variable(tf.truncated_normal([256, 128],
                                                stddev=0.1,
                                                seed=SEED,
                                                dtype=data_type()))
fc3_biases = tf.Variable(tf.constant(
      0.1, shape=[128], dtype=data_type()))

fc4_weights = tf.Variable(tf.truncated_normal([128, NUM_LABELS],
                                                stddev=0.1,
                                                seed=SEED,
                                                dtype=data_type()))
fc4_biases = tf.Variable(tf.constant(
      0.1, shape=[NUM_LABELS], dtype=data_type()))


train_data_node = tf.placeholder(
        data_type(),
        shape=(None, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))
train_labels_node = tf.placeholder(tf.int64, shape=(None,))
eval_data = tf.placeholder(
        data_type(),
        shape=(None, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))

conv1_weights = tf.Variable(
        tf.truncated_normal([5, 5, NUM_CHANNELS, 32],
                stddev=0.1,
                seed=SEED, dtype=data_type()))
conv1_biases = tf.Variable(tf.zeros([32], dtype=data_type()))
conv2_weights = tf.Variable(tf.truncated_normal(
        [5, 5, 32, 64], stddev=0.1,
        seed=SEED, dtype=data_type()))
conv2_biases = tf.Variable(tf.constant(0.1, shape=[64], dtype=data_type()))

conv3_weights = tf.Variable(tf.truncated_normal(
        [5, 5, 64, 128], stddev=0.1,
        seed=SEED, dtype=data_type()))
conv3_biases = tf.Variable(tf.constant(0.1, shape=[128], dtype=data_type()))

fc1_weights = tf.Variable(
        tf.truncated_normal([IMAGE_SIZE // 8 * IMAGE_SIZE // 8 * 128, 512],
                stddev=0.1,
                seed=SEED,
                dtype=data_type()))
fc1_biases = tf.Variable(tf.constant(0.1, shape=[512], dtype=data_type()))
fc2_weights = tf.Variable(tf.truncated_normal([512, 256],
        stddev=0.1,
        seed=SEED,
        dtype=data_type()))

fc2_biases = tf.Variable(tf.constant(
        0.1, shape=[256], dtype=data_type()))

fc3_weights = tf.Variable(tf.truncated_normal([256, 128],
        stddev=0.1,
        seed=SEED,
        dtype=data_type()))
fc3_biases = tf.Variable(tf.constant(
        0.1, shape=[128], dtype=data_type()))

fc4_weights = tf.Variable(tf.truncated_normal([128, NUM_LABELS],
        stddev=0.1,
        seed=SEED,
        dtype=data_type()))
fc4_biases = tf.Variable(tf.constant(
        0.1, shape=[NUM_LABELS], dtype=data_type()))

def model(data, train=False):
    """The Model definition."""
    conv = tf.nn.conv2d(data,
                        conv1_weights,
                        strides=[1, 1, 1, 1],
                        padding='SAME')
    relu = tf.nn.relu(tf.nn.bias_add(conv, conv1_biases))
    pool = tf.nn.max_pool(relu,
                          ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1],
                          padding='SAME')

    conv = tf.nn.conv2d(pool,
                        conv2_weights,
                        strides=[1, 1, 1, 1],
                        padding='SAME')
    relu = tf.nn.relu(tf.nn.bias_add(conv, conv2_biases))
    pool = tf.nn.max_pool(relu,
                          ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1],
                          padding='SAME')

    conv = tf.nn.conv2d(pool,
                        conv3_weights,
                        strides=[1, 1, 1, 1],
                        padding='SAME')
    relu = tf.nn.relu(tf.nn.bias_add(conv, conv3_biases))
    pool = tf.nn.max_pool(relu,
                          ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1],
                          padding='SAME')

    reshape = flatten(pool)
    fc1 = tf.nn.relu(tf.matmul(reshape, fc1_weights) + fc1_biases)
    fc2 = tf.nn.relu(tf.matmul(fc1, fc2_weights) + fc2_biases)
    fc3 = tf.nn.relu(tf.matmul(fc2, fc3_weights) + fc3_biases)

    if train:
        fc3 = tf.nn.dropout(fc3, 0.5, seed=SEED)
    return tf.matmul(fc3, fc4_weights) + fc4_biases

logits = model(train_data_node, True)
loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, train_labels_node))

regularizers = (tf.nn.l2_loss(fc1_weights) + tf.nn.l2_loss(fc1_biases) +
                  tf.nn.l2_loss(fc2_weights) + tf.nn.l2_loss(fc2_biases))
loss += 5e-3 * regularizers

optimizer = tf.train.AdamOptimizer(0.001).minimize(loss)
train_prediction = tf.nn.softmax(logits)
eval_prediction = tf.nn.softmax(model(eval_data))

def eval_in_batches(data, sess):
    """Get all predictions for a dataset by running it in small batches."""
    size = data.shape[0]
    predictions = numpy.ndarray(shape=(size, NUM_LABELS), dtype=numpy.float32)
    for begin in xrange(0, size, BATCH_SIZE):
        end = begin + BATCH_SIZE
        if end <= size:
            batch_data = normalize_data(data[begin:end, ...])
            predictions[begin:end, :] = sess.run(
                eval_prediction,
                feed_dict={eval_data: batch_data})
        else:
            batch_data = normalize_data(data[-BATCH_SIZE:, ...])
            batch_predictions = sess.run(
                eval_prediction,
                feed_dict={eval_data: batch_data})
            predictions[begin:, :] = batch_predictions[begin - size:, :]
    return predictions

def error_rate(predictions, labels):
  """Return the error rate based on dense predictions and sparse labels."""
  return 100.0 - (
      100.0 *
      numpy.sum(numpy.argmax(predictions, 1) == labels) /
      predictions.shape[0])

def resizeImage32x32(folder, file_name):
    with open(folder + "/" + file_name, 'r+b') as f:
        with Image.open(f) as image:
            image = image.resize((32, 32), Image.ANTIALIAS)
            return numpy.array(image.convert("RGB"))


def openImagesAtPath(folder_path):
    image_files = [ f for f in listdir(folder_path) if (isfile(join(folder_path,f)) and (f.endswith(".png") or f.endswith(".jpg"))) ]
    images = numpy.empty(len(image_files), dtype=object)
    for n in range(0, len(image_files)):
        print(image_files[n])
        images[n] = resizeImage32x32(folder_path, image_files[n])
    return image_files, numpy.array([i for i in images])


### Train your model here.
### Feel free to use as many code cells as needed.
with tf.Session() as sess:
    tf.global_variables_initializer().run()
    print('Initialized!')

    for i in range(0, NUM_EPOCHS):
        train_data, train_labels = shuffle(train_data, train_labels)
        start_time = time.time()
        train_size = len(train_data)

        for offset in range(0, train_size, BATCH_SIZE):
            end = offset + BATCH_SIZE
            batch_data, batch_labels = train_data[offset:end], train_labels[offset:end]
            batch_data = normalize_data(batch_data)
            feed_dict = {train_data_node: batch_data, train_labels_node: batch_labels}
            sess.run(optimizer, feed_dict=feed_dict)

        l, predictions = sess.run([loss, train_prediction], feed_dict=feed_dict)
        elapsed_time = time.time() - start_time

        print('Epoch %d, %.1f seconds' % (i, elapsed_time))
        print('Minibatch loss: %.3f' % l)
        print('Minibatch error: %.1f%%' % error_rate(predictions, batch_labels))
        print('Validation error: %.1f%%' % error_rate(
            eval_in_batches(validation_data, sess), validation_labels))
        sys.stdout.flush()

    test_error = error_rate(eval_in_batches(test_data, sess), test_labels)
    print('Test error: %.1f%%' % test_error)

    # test my images downloaded from internet
    image_names, images = openImagesAtPath("my_images/")
    images = normalize_data(images)

    test_top_k = sess.run(tf.nn.top_k(tf.nn.softmax(model(images)), 5), feed_dict={eval_data: images})
    values, indices = test_top_k.values, test_top_k.indices
    print(values)
    print(indices)

    for i in range(0, len(image_names)):
        fig = plt.figure(i)
        fig.canvas.set_window_title('Image - %d' % i)
        bar = plt.bar(indices[i], values[i],
                      label=('%d' % i), color=numpy.random.rand(3, 1), width=1)
        plt.xlabel('Label for %s' % image_names[i])
        plt.ylabel('Probability')
    plt.show()

### Load the images and plot them here.
### Feel free to use as many code cells as needed.
names, images = openImagesAtPath("my_images/")
for img in images:
    plt.imshow(img)
    plt.show()